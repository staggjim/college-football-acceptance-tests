package college.football.api.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.*;

public class CollegeFootballStepDefs {

    private Response response;
    private RequestSpecification requestSpec = RestAssured.given();
    private String collegeFootballURL = "https://api.collegefootballdata.com/";
    private String baseURI, firstNameQueried, lastNameQueried, teamQueried, currentResourcePath;
    private int yearQueried;

    /*********
     * GIVEN *
     ********/
    @Given("^API resource path is \"([^\"]*)\"$")
    public void apiResourcePathIs(String resourcePath) {
        currentResourcePath = resourcePath;
        baseURI = collegeFootballURL + resourcePath;
    }

    /********
     * WHEN *
     ********/
    @And("^I trigger the request$")
    public void iTriggerTheRequest() {
        response = requestSpec.log().all().get(baseURI);
    }

    @When("^I add all required query parameters \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
    public void iAddAllRequiredQueryParameters(String seasonType, String year, String week, String team, String conference, String matchupTeams, String coach) {
        if (!seasonType.equals("null")) {
            requestSpec.queryParam("seasonType", seasonType);
        }
        if (!year.equals("null")) {
            requestSpec.queryParam("year", year);
        }
        if (!week.equals("null")) {
            requestSpec.queryParam("week", week);
        }
        if (!team.equals("null")) {
            requestSpec.queryParam("team", team);
        }
        if (!conference.equals("null")) {
            requestSpec.queryParam("conference", conference);
        }
        if (!matchupTeams.equals("null")) {
            String[] teamsList = matchupTeams.split(",");
            String team1 = teamsList[0];
            String team2 = teamsList[1];
            requestSpec.queryParam("team1", team1);
            requestSpec.queryParam("team2", team2);
        }
        if (!coach.equals("null")) {
            requestSpec.queryParam("firstName", coach);
        }
    }

    @When("^I do not add all required query parameters \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
    public void iDoNotAddAllRequiredQueryParameters(String seasonType, String year, String week, String team, String conference, String matchupTeams, String coach) {
        if (!seasonType.equals("null")) {
            requestSpec.queryParam("seasonType", seasonType);
        }
        if (!year.equals("null")) {
            requestSpec.queryParam("year", year);
        }
        if (!week.equals("null")) {
            requestSpec.queryParam("week", week);
        }
        if (!team.equals("null")) {
            requestSpec.queryParam("team", team);
        }
        if (!conference.equals("null")) {
            requestSpec.queryParam("conference", conference);
        }
        if (!matchupTeams.equals("null")) {
            String[] teamsList = matchupTeams.split(",");
            String team1 = teamsList[0];
            String team2 = teamsList[1];
            requestSpec.queryParam("team1", team1);
            requestSpec.queryParam("team2", team2);
        }
        if (!coach.equals("null")) {
            requestSpec.queryParam("firstName", coach);
        }
    }

    @When("^I add all required query parameters to get coaches request \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
    public void iAddAllRequiredQueryParametersToGetCoachesRequest(String firstName, String lastName, String team, String year) {
        firstNameQueried = firstName;
        lastNameQueried = lastName;
        teamQueried = team;
        yearQueried = Integer.parseInt(year);
        if (!firstName.equals("null")) {
            requestSpec.queryParam("firstName", firstName);
        }
        if (!lastName.equals("null")) {
            requestSpec.queryParam("lastName", lastName);
        }
        if (!team.equals("null")) {
            requestSpec.queryParam("team", team);
        }
        if (!year.equals("null")) {
            requestSpec.queryParam("year", year);
        }
    }

    /********
     * THEN *
     ********/
    @Then("^The client receives response status code of (\\d+)$")
    public void theClientReceivesStatusCodeOf(int statusCode) {
        response.then().log().all().statusCode(statusCode);
    }

    @Then("^The response content type is as expected")
    public void theResponseContentTypeIsAsExpected() {
        if(currentResourcePath.equals("games/players") || currentResourcePath.equals("games/teams")){
            response.then().assertThat().contentType(ContentType.HTML);
        } else
            response.then().assertThat().contentType(ContentType.JSON);
    }

    @Then("^The response content type is json")
    public void theResponseContentTypeIsJson() {
        if(currentResourcePath.equals("games/players") || currentResourcePath.equals("games/teams")){
            response.then().assertThat().contentType(ContentType.HTML);
        } else
            response.then().assertThat().contentType(ContentType.JSON);
    }

    @And("The number of results returned is correct")
    public void theNumberOfResultsReturnedIsCorrect() {
        int numResultsExp = 1;
        verifyNumberResults(response.jsonPath(), numResultsExp);
    }

    @And("^The error message returned is as expected \"([^\"]*)\"$")
    public void theErrorMessageReturnedIsAsExpected(String expectedResponseBody) {
        assertResponseBodyIsCorrect(response.getBody().asString().replace("\"",""), expectedResponseBody);
    }

    @And("^The response result matches the search criteria$")
    public void theResponseResultMatchesTheSearchCriteria() {
        response.then().assertThat()
                .body("first_name", hasItem(firstNameQueried))
                .body("last_name", hasItem(lastNameQueried))
                .body("seasons.school[0]", hasItem(teamQueried))
                .body("seasons.year[0]", hasItem(2019));
    }

    private void assertResponseBodyIsCorrect(String actualResponseBody, String expectedResponseBody) {
        Assert.assertEquals(actualResponseBody, expectedResponseBody);
    }

    public static void verifyNumberResults(JsonPath jsonPath, int numResultsExp) {
        List<String> drivers = jsonPath.getList("items");
        int numResults = drivers.size();
        assertThat(numResults).withFailMessage("Assertion of number results returned has failed. Expected: " + numResultsExp + " Actual: " + numResults).isEqualTo(numResultsExp);
    }
}
