##Tests Overview:

##Scenario Outline: 1.1_Verify successful response from all College Football Resources
1. Added this test to verify success response from all endpoints
2. When implementing full test framework I would store the test data(query parameters etc.) in an excel spreadsheet and
input it to the test cases from there.

##Scenario Outline: 1.2_Verify get coaches endpoint returns correct details as per query parameters
This test verifies that the response returned by the etCoaches endpoint is correct based on the query parameters used

##Scenario Outline: 1.3_Verify 400 error is returned for request missing required parameters
This test verifies 400 errors returned when required query parameters are missing

##Scenario Outline: 1.4_Verify 400 error is returned for request with parameters formatting errors
This test verifies 400 errors returned when query parameters contain formatting errors

##HOW TO INSTALL RUN AND WRITE NEW TESTS

INSTALL:
Clone project from gitlab to local machine
Open the project in your IDE
Run the mvn install command from the terminal (this will build and also run tests)

RUN TESTS:
mvn clean verify
or
Run 1_College Football API Tests.feature file

ADD TESTS
New tests can be added to the existing feature file or a new feature file
New rows can be added to the examples section to add new tests

##ENDPOINT INTERACTIONS LIST:

getGames
getPlayerGameStats
getTeamGameStats
getDrives
getPlays
getPlayTypes
getTeams
getRoster
getTalent
getTeams
getTeamMatchup
getConferences
getVenues
getCoaches
getRankings


##TEST REPORTS
Cucumber test reports are generated at the end of each test run. Results can be found in
the '\target\cucumber-report-html\cucumber-html-reports' directory
