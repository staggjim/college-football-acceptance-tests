$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("1_College Football API Tests.feature");
formatter.feature({
  "line": 2,
  "name": "College Football API_Acceptance Tests",
  "description": "Description: Verify all endpoints work as expected",
  "id": "college-football-api-acceptance-tests",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@TestSet"
    }
  ]
});
formatter.scenarioOutline({
  "line": 6,
  "name": "1.1_Verify successful response from all College Football Resources",
  "description": "",
  "id": "college-football-api-acceptance-tests;1.1-verify-successful-response-from-all-college-football-resources",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 7,
  "name": "API resource path is \"\u003cEndpoint\u003e\"",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "I add all required query parameters \"\u003cSeasonType\u003e\" \"\u003cYear\u003e\" \"\u003cWeek\u003e\" \"\u003cTeam\u003e\" \"\u003cConference\u003e\" \"\u003cMatchupTeams\u003e\" \"\u003cCoach\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "I trigger the request",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "The client receives response status code of 200",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "The response content type is as expected",
  "keyword": "And "
});
formatter.examples({
  "line": 12,
  "name": "",
  "description": "",
  "id": "college-football-api-acceptance-tests;1.1-verify-successful-response-from-all-college-football-resources;",
  "rows": [
    {
      "cells": [
        "Endpoint",
        "SeasonType",
        "Year",
        "Week",
        "Team",
        "Conference",
        "MatchupTeams",
        "Coach"
      ],
      "line": 13,
      "id": "college-football-api-acceptance-tests;1.1-verify-successful-response-from-all-college-football-resources;;1"
    },
    {
      "cells": [
        "games",
        "regular",
        "2019",
        "null",
        "null",
        "null",
        "null",
        "null"
      ],
      "line": 14,
      "id": "college-football-api-acceptance-tests;1.1-verify-successful-response-from-all-college-football-resources;;2"
    },
    {
      "cells": [
        "games/players",
        "regular",
        "2019",
        "null",
        "null",
        "null",
        "null",
        "null"
      ],
      "line": 15,
      "id": "college-football-api-acceptance-tests;1.1-verify-successful-response-from-all-college-football-resources;;3"
    },
    {
      "cells": [
        "games/teams",
        "regular",
        "2019",
        "null",
        "null",
        "null",
        "null",
        "null"
      ],
      "line": 16,
      "id": "college-football-api-acceptance-tests;1.1-verify-successful-response-from-all-college-football-resources;;4"
    },
    {
      "cells": [
        "drives",
        "regular",
        "2019",
        "null",
        "null",
        "null",
        "null",
        "null"
      ],
      "line": 17,
      "id": "college-football-api-acceptance-tests;1.1-verify-successful-response-from-all-college-football-resources;;5"
    },
    {
      "cells": [
        "play/types",
        "null",
        "null",
        "null",
        "null",
        "null",
        "null",
        "null"
      ],
      "line": 18,
      "id": "college-football-api-acceptance-tests;1.1-verify-successful-response-from-all-college-football-resources;;6"
    },
    {
      "cells": [
        "plays",
        "regular",
        "2019",
        "1",
        "null",
        "null",
        "null",
        "null"
      ],
      "line": 19,
      "id": "college-football-api-acceptance-tests;1.1-verify-successful-response-from-all-college-football-resources;;7"
    },
    {
      "cells": [
        "roster",
        "null",
        "2019",
        "null",
        "auburn",
        "null",
        "null",
        "null"
      ],
      "line": 20,
      "id": "college-football-api-acceptance-tests;1.1-verify-successful-response-from-all-college-football-resources;;8"
    },
    {
      "cells": [
        "talent",
        "null",
        "2019",
        "null",
        "null",
        "null",
        "null",
        "null"
      ],
      "line": 21,
      "id": "college-football-api-acceptance-tests;1.1-verify-successful-response-from-all-college-football-resources;;9"
    },
    {
      "cells": [
        "teams",
        "null",
        "null",
        "null",
        "null",
        "ACC",
        "null",
        "null"
      ],
      "line": 22,
      "id": "college-football-api-acceptance-tests;1.1-verify-successful-response-from-all-college-football-resources;;10"
    },
    {
      "cells": [
        "teams/matchup",
        "null",
        "null",
        "null",
        "null",
        "null",
        "auburn,michigan",
        "null"
      ],
      "line": 23,
      "id": "college-football-api-acceptance-tests;1.1-verify-successful-response-from-all-college-football-resources;;11"
    },
    {
      "cells": [
        "conferences",
        "null",
        "null",
        "null",
        "null",
        "null",
        "null",
        "null"
      ],
      "line": 24,
      "id": "college-football-api-acceptance-tests;1.1-verify-successful-response-from-all-college-football-resources;;12"
    },
    {
      "cells": [
        "venues",
        "null",
        "null",
        "null",
        "null",
        "null",
        "null",
        "null"
      ],
      "line": 25,
      "id": "college-football-api-acceptance-tests;1.1-verify-successful-response-from-all-college-football-resources;;13"
    },
    {
      "cells": [
        "coaches",
        "null",
        "null",
        "null",
        "null",
        "null",
        "null",
        "meyer"
      ],
      "line": 26,
      "id": "college-football-api-acceptance-tests;1.1-verify-successful-response-from-all-college-football-resources;;14"
    },
    {
      "cells": [
        "rankings",
        "regular",
        "2019",
        "null",
        "null",
        "null",
        "null",
        "null"
      ],
      "line": 27,
      "id": "college-football-api-acceptance-tests;1.1-verify-successful-response-from-all-college-football-resources;;15"
    },
    {
      "cells": [
        "venues",
        "null",
        "null",
        "null",
        "null",
        "null",
        "null",
        "null"
      ],
      "line": 28,
      "id": "college-football-api-acceptance-tests;1.1-verify-successful-response-from-all-college-football-resources;;16"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 14,
  "name": "1.1_Verify successful response from all College Football Resources",
  "description": "",
  "id": "college-football-api-acceptance-tests;1.1-verify-successful-response-from-all-college-football-resources;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@TestSet"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "API resource path is \"games\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "I add all required query parameters \"regular\" \"2019\" \"null\" \"null\" \"null\" \"null\" \"null\"",
  "matchedColumns": [
    1,
    2,
    3,
    4,
    5,
    6,
    7
  ],
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "I trigger the request",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "The client receives response status code of 200",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "The response content type is as expected",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "games",
      "offset": 22
    }
  ],
  "location": "CollegeFootballStepDefs.apiResourcePathIs(String)"
});
formatter.result({
  "duration": 1142145100,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "regular",
      "offset": 37
    },
    {
      "val": "2019",
      "offset": 47
    },
    {
      "val": "null",
      "offset": 54
    },
    {
      "val": "null",
      "offset": 61
    },
    {
      "val": "null",
      "offset": 68
    },
    {
      "val": "null",
      "offset": 75
    },
    {
      "val": "null",
      "offset": 82
    }
  ],
  "location": "CollegeFootballStepDefs.iAddAllRequiredQueryParameters(String,String,String,String,String,String,String)"
});
formatter.result({
  "duration": 52254100,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.iTriggerTheRequest()"
});
formatter.result({
  "duration": 3743468200,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 44
    }
  ],
  "location": "CollegeFootballStepDefs.theClientReceivesStatusCodeOf(int)"
});
formatter.result({
  "duration": 958247400,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.theResponseContentTypeIsAsExpected()"
});
formatter.result({
  "duration": 6810400,
  "status": "passed"
});
formatter.scenario({
  "line": 15,
  "name": "1.1_Verify successful response from all College Football Resources",
  "description": "",
  "id": "college-football-api-acceptance-tests;1.1-verify-successful-response-from-all-college-football-resources;;3",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@TestSet"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "API resource path is \"games/players\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "I add all required query parameters \"regular\" \"2019\" \"null\" \"null\" \"null\" \"null\" \"null\"",
  "matchedColumns": [
    1,
    2,
    3,
    4,
    5,
    6,
    7
  ],
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "I trigger the request",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "The client receives response status code of 200",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "The response content type is as expected",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "games/players",
      "offset": 22
    }
  ],
  "location": "CollegeFootballStepDefs.apiResourcePathIs(String)"
});
formatter.result({
  "duration": 461500,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "regular",
      "offset": 37
    },
    {
      "val": "2019",
      "offset": 47
    },
    {
      "val": "null",
      "offset": 54
    },
    {
      "val": "null",
      "offset": 61
    },
    {
      "val": "null",
      "offset": 68
    },
    {
      "val": "null",
      "offset": 75
    },
    {
      "val": "null",
      "offset": 82
    }
  ],
  "location": "CollegeFootballStepDefs.iAddAllRequiredQueryParameters(String,String,String,String,String,String,String)"
});
formatter.result({
  "duration": 713700,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.iTriggerTheRequest()"
});
formatter.result({
  "duration": 687854300,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 44
    }
  ],
  "location": "CollegeFootballStepDefs.theClientReceivesStatusCodeOf(int)"
});
formatter.result({
  "duration": 419819500,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.theResponseContentTypeIsAsExpected()"
});
formatter.result({
  "duration": 3642900,
  "status": "passed"
});
formatter.scenario({
  "line": 16,
  "name": "1.1_Verify successful response from all College Football Resources",
  "description": "",
  "id": "college-football-api-acceptance-tests;1.1-verify-successful-response-from-all-college-football-resources;;4",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@TestSet"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "API resource path is \"games/teams\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "I add all required query parameters \"regular\" \"2019\" \"null\" \"null\" \"null\" \"null\" \"null\"",
  "matchedColumns": [
    1,
    2,
    3,
    4,
    5,
    6,
    7
  ],
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "I trigger the request",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "The client receives response status code of 200",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "The response content type is as expected",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "games/teams",
      "offset": 22
    }
  ],
  "location": "CollegeFootballStepDefs.apiResourcePathIs(String)"
});
formatter.result({
  "duration": 1496800,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "regular",
      "offset": 37
    },
    {
      "val": "2019",
      "offset": 47
    },
    {
      "val": "null",
      "offset": 54
    },
    {
      "val": "null",
      "offset": 61
    },
    {
      "val": "null",
      "offset": 68
    },
    {
      "val": "null",
      "offset": 75
    },
    {
      "val": "null",
      "offset": 82
    }
  ],
  "location": "CollegeFootballStepDefs.iAddAllRequiredQueryParameters(String,String,String,String,String,String,String)"
});
formatter.result({
  "duration": 490900,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.iTriggerTheRequest()"
});
formatter.result({
  "duration": 705173900,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 44
    }
  ],
  "location": "CollegeFootballStepDefs.theClientReceivesStatusCodeOf(int)"
});
formatter.result({
  "duration": 7062900,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.theResponseContentTypeIsAsExpected()"
});
formatter.result({
  "duration": 1320100,
  "status": "passed"
});
formatter.scenario({
  "line": 17,
  "name": "1.1_Verify successful response from all College Football Resources",
  "description": "",
  "id": "college-football-api-acceptance-tests;1.1-verify-successful-response-from-all-college-football-resources;;5",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@TestSet"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "API resource path is \"drives\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "I add all required query parameters \"regular\" \"2019\" \"null\" \"null\" \"null\" \"null\" \"null\"",
  "matchedColumns": [
    1,
    2,
    3,
    4,
    5,
    6,
    7
  ],
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "I trigger the request",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "The client receives response status code of 200",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "The response content type is as expected",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "drives",
      "offset": 22
    }
  ],
  "location": "CollegeFootballStepDefs.apiResourcePathIs(String)"
});
formatter.result({
  "duration": 499500,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "regular",
      "offset": 37
    },
    {
      "val": "2019",
      "offset": 47
    },
    {
      "val": "null",
      "offset": 54
    },
    {
      "val": "null",
      "offset": 61
    },
    {
      "val": "null",
      "offset": 68
    },
    {
      "val": "null",
      "offset": 75
    },
    {
      "val": "null",
      "offset": 82
    }
  ],
  "location": "CollegeFootballStepDefs.iAddAllRequiredQueryParameters(String,String,String,String,String,String,String)"
});
formatter.result({
  "duration": 616000,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.iTriggerTheRequest()"
});
formatter.result({
  "duration": 1765175200,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 44
    }
  ],
  "location": "CollegeFootballStepDefs.theClientReceivesStatusCodeOf(int)"
});
formatter.result({
  "duration": 13254377900,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.theResponseContentTypeIsAsExpected()"
});
formatter.result({
  "duration": 936000,
  "status": "passed"
});
formatter.scenario({
  "line": 18,
  "name": "1.1_Verify successful response from all College Football Resources",
  "description": "",
  "id": "college-football-api-acceptance-tests;1.1-verify-successful-response-from-all-college-football-resources;;6",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@TestSet"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "API resource path is \"play/types\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "I add all required query parameters \"null\" \"null\" \"null\" \"null\" \"null\" \"null\" \"null\"",
  "matchedColumns": [
    1,
    2,
    3,
    4,
    5,
    6,
    7
  ],
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "I trigger the request",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "The client receives response status code of 200",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "The response content type is as expected",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "play/types",
      "offset": 22
    }
  ],
  "location": "CollegeFootballStepDefs.apiResourcePathIs(String)"
});
formatter.result({
  "duration": 415900,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "null",
      "offset": 37
    },
    {
      "val": "null",
      "offset": 44
    },
    {
      "val": "null",
      "offset": 51
    },
    {
      "val": "null",
      "offset": 58
    },
    {
      "val": "null",
      "offset": 65
    },
    {
      "val": "null",
      "offset": 72
    },
    {
      "val": "null",
      "offset": 79
    }
  ],
  "location": "CollegeFootballStepDefs.iAddAllRequiredQueryParameters(String,String,String,String,String,String,String)"
});
formatter.result({
  "duration": 189500,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.iTriggerTheRequest()"
});
formatter.result({
  "duration": 627462200,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 44
    }
  ],
  "location": "CollegeFootballStepDefs.theClientReceivesStatusCodeOf(int)"
});
formatter.result({
  "duration": 2213100,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.theResponseContentTypeIsAsExpected()"
});
formatter.result({
  "duration": 1162300,
  "status": "passed"
});
formatter.scenario({
  "line": 19,
  "name": "1.1_Verify successful response from all College Football Resources",
  "description": "",
  "id": "college-football-api-acceptance-tests;1.1-verify-successful-response-from-all-college-football-resources;;7",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@TestSet"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "API resource path is \"plays\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "I add all required query parameters \"regular\" \"2019\" \"1\" \"null\" \"null\" \"null\" \"null\"",
  "matchedColumns": [
    1,
    2,
    3,
    4,
    5,
    6,
    7
  ],
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "I trigger the request",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "The client receives response status code of 200",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "The response content type is as expected",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "plays",
      "offset": 22
    }
  ],
  "location": "CollegeFootballStepDefs.apiResourcePathIs(String)"
});
formatter.result({
  "duration": 2783200,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "regular",
      "offset": 37
    },
    {
      "val": "2019",
      "offset": 47
    },
    {
      "val": "1",
      "offset": 54
    },
    {
      "val": "null",
      "offset": 58
    },
    {
      "val": "null",
      "offset": 65
    },
    {
      "val": "null",
      "offset": 72
    },
    {
      "val": "null",
      "offset": 79
    }
  ],
  "location": "CollegeFootballStepDefs.iAddAllRequiredQueryParameters(String,String,String,String,String,String,String)"
});
formatter.result({
  "duration": 1968800,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.iTriggerTheRequest()"
});
formatter.result({
  "duration": 1378586800,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 44
    }
  ],
  "location": "CollegeFootballStepDefs.theClientReceivesStatusCodeOf(int)"
});
formatter.result({
  "duration": 4858505900,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.theResponseContentTypeIsAsExpected()"
});
formatter.result({
  "duration": 1615900,
  "status": "passed"
});
formatter.scenario({
  "line": 20,
  "name": "1.1_Verify successful response from all College Football Resources",
  "description": "",
  "id": "college-football-api-acceptance-tests;1.1-verify-successful-response-from-all-college-football-resources;;8",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@TestSet"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "API resource path is \"roster\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "I add all required query parameters \"null\" \"2019\" \"null\" \"auburn\" \"null\" \"null\" \"null\"",
  "matchedColumns": [
    1,
    2,
    3,
    4,
    5,
    6,
    7
  ],
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "I trigger the request",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "The client receives response status code of 200",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "The response content type is as expected",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "roster",
      "offset": 22
    }
  ],
  "location": "CollegeFootballStepDefs.apiResourcePathIs(String)"
});
formatter.result({
  "duration": 392100,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "null",
      "offset": 37
    },
    {
      "val": "2019",
      "offset": 44
    },
    {
      "val": "null",
      "offset": 51
    },
    {
      "val": "auburn",
      "offset": 58
    },
    {
      "val": "null",
      "offset": 67
    },
    {
      "val": "null",
      "offset": 74
    },
    {
      "val": "null",
      "offset": 81
    }
  ],
  "location": "CollegeFootballStepDefs.iAddAllRequiredQueryParameters(String,String,String,String,String,String,String)"
});
formatter.result({
  "duration": 620000,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.iTriggerTheRequest()"
});
formatter.result({
  "duration": 708686100,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 44
    }
  ],
  "location": "CollegeFootballStepDefs.theClientReceivesStatusCodeOf(int)"
});
formatter.result({
  "duration": 12275100,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.theResponseContentTypeIsAsExpected()"
});
formatter.result({
  "duration": 689500,
  "status": "passed"
});
formatter.scenario({
  "line": 21,
  "name": "1.1_Verify successful response from all College Football Resources",
  "description": "",
  "id": "college-football-api-acceptance-tests;1.1-verify-successful-response-from-all-college-football-resources;;9",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@TestSet"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "API resource path is \"talent\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "I add all required query parameters \"null\" \"2019\" \"null\" \"null\" \"null\" \"null\" \"null\"",
  "matchedColumns": [
    1,
    2,
    3,
    4,
    5,
    6,
    7
  ],
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "I trigger the request",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "The client receives response status code of 200",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "The response content type is as expected",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "talent",
      "offset": 22
    }
  ],
  "location": "CollegeFootballStepDefs.apiResourcePathIs(String)"
});
formatter.result({
  "duration": 747100,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "null",
      "offset": 37
    },
    {
      "val": "2019",
      "offset": 44
    },
    {
      "val": "null",
      "offset": 51
    },
    {
      "val": "null",
      "offset": 58
    },
    {
      "val": "null",
      "offset": 65
    },
    {
      "val": "null",
      "offset": 72
    },
    {
      "val": "null",
      "offset": 79
    }
  ],
  "location": "CollegeFootballStepDefs.iAddAllRequiredQueryParameters(String,String,String,String,String,String,String)"
});
formatter.result({
  "duration": 284200,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.iTriggerTheRequest()"
});
formatter.result({
  "duration": 621988500,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 44
    }
  ],
  "location": "CollegeFootballStepDefs.theClientReceivesStatusCodeOf(int)"
});
formatter.result({
  "duration": 8304700,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.theResponseContentTypeIsAsExpected()"
});
formatter.result({
  "duration": 5615100,
  "status": "passed"
});
formatter.scenario({
  "line": 22,
  "name": "1.1_Verify successful response from all College Football Resources",
  "description": "",
  "id": "college-football-api-acceptance-tests;1.1-verify-successful-response-from-all-college-football-resources;;10",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@TestSet"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "API resource path is \"teams\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "I add all required query parameters \"null\" \"null\" \"null\" \"null\" \"ACC\" \"null\" \"null\"",
  "matchedColumns": [
    1,
    2,
    3,
    4,
    5,
    6,
    7
  ],
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "I trigger the request",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "The client receives response status code of 200",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "The response content type is as expected",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "teams",
      "offset": 22
    }
  ],
  "location": "CollegeFootballStepDefs.apiResourcePathIs(String)"
});
formatter.result({
  "duration": 580800,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "null",
      "offset": 37
    },
    {
      "val": "null",
      "offset": 44
    },
    {
      "val": "null",
      "offset": 51
    },
    {
      "val": "null",
      "offset": 58
    },
    {
      "val": "ACC",
      "offset": 65
    },
    {
      "val": "null",
      "offset": 71
    },
    {
      "val": "null",
      "offset": 78
    }
  ],
  "location": "CollegeFootballStepDefs.iAddAllRequiredQueryParameters(String,String,String,String,String,String,String)"
});
formatter.result({
  "duration": 762000,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.iTriggerTheRequest()"
});
formatter.result({
  "duration": 629401400,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 44
    }
  ],
  "location": "CollegeFootballStepDefs.theClientReceivesStatusCodeOf(int)"
});
formatter.result({
  "duration": 3804100,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.theResponseContentTypeIsAsExpected()"
});
formatter.result({
  "duration": 583900,
  "status": "passed"
});
formatter.scenario({
  "line": 23,
  "name": "1.1_Verify successful response from all College Football Resources",
  "description": "",
  "id": "college-football-api-acceptance-tests;1.1-verify-successful-response-from-all-college-football-resources;;11",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@TestSet"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "API resource path is \"teams/matchup\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "I add all required query parameters \"null\" \"null\" \"null\" \"null\" \"null\" \"auburn,michigan\" \"null\"",
  "matchedColumns": [
    1,
    2,
    3,
    4,
    5,
    6,
    7
  ],
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "I trigger the request",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "The client receives response status code of 200",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "The response content type is as expected",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "teams/matchup",
      "offset": 22
    }
  ],
  "location": "CollegeFootballStepDefs.apiResourcePathIs(String)"
});
formatter.result({
  "duration": 307700,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "null",
      "offset": 37
    },
    {
      "val": "null",
      "offset": 44
    },
    {
      "val": "null",
      "offset": 51
    },
    {
      "val": "null",
      "offset": 58
    },
    {
      "val": "null",
      "offset": 65
    },
    {
      "val": "auburn,michigan",
      "offset": 72
    },
    {
      "val": "null",
      "offset": 90
    }
  ],
  "location": "CollegeFootballStepDefs.iAddAllRequiredQueryParameters(String,String,String,String,String,String,String)"
});
formatter.result({
  "duration": 1060500,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.iTriggerTheRequest()"
});
formatter.result({
  "duration": 614187300,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 44
    }
  ],
  "location": "CollegeFootballStepDefs.theClientReceivesStatusCodeOf(int)"
});
formatter.result({
  "duration": 1443400,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.theResponseContentTypeIsAsExpected()"
});
formatter.result({
  "duration": 581100,
  "status": "passed"
});
formatter.scenario({
  "line": 24,
  "name": "1.1_Verify successful response from all College Football Resources",
  "description": "",
  "id": "college-football-api-acceptance-tests;1.1-verify-successful-response-from-all-college-football-resources;;12",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@TestSet"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "API resource path is \"conferences\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "I add all required query parameters \"null\" \"null\" \"null\" \"null\" \"null\" \"null\" \"null\"",
  "matchedColumns": [
    1,
    2,
    3,
    4,
    5,
    6,
    7
  ],
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "I trigger the request",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "The client receives response status code of 200",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "The response content type is as expected",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "conferences",
      "offset": 22
    }
  ],
  "location": "CollegeFootballStepDefs.apiResourcePathIs(String)"
});
formatter.result({
  "duration": 313000,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "null",
      "offset": 37
    },
    {
      "val": "null",
      "offset": 44
    },
    {
      "val": "null",
      "offset": 51
    },
    {
      "val": "null",
      "offset": 58
    },
    {
      "val": "null",
      "offset": 65
    },
    {
      "val": "null",
      "offset": 72
    },
    {
      "val": "null",
      "offset": 79
    }
  ],
  "location": "CollegeFootballStepDefs.iAddAllRequiredQueryParameters(String,String,String,String,String,String,String)"
});
formatter.result({
  "duration": 142400,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.iTriggerTheRequest()"
});
formatter.result({
  "duration": 580396100,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 44
    }
  ],
  "location": "CollegeFootballStepDefs.theClientReceivesStatusCodeOf(int)"
});
formatter.result({
  "duration": 1987500,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.theResponseContentTypeIsAsExpected()"
});
formatter.result({
  "duration": 611300,
  "status": "passed"
});
formatter.scenario({
  "line": 25,
  "name": "1.1_Verify successful response from all College Football Resources",
  "description": "",
  "id": "college-football-api-acceptance-tests;1.1-verify-successful-response-from-all-college-football-resources;;13",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@TestSet"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "API resource path is \"venues\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "I add all required query parameters \"null\" \"null\" \"null\" \"null\" \"null\" \"null\" \"null\"",
  "matchedColumns": [
    1,
    2,
    3,
    4,
    5,
    6,
    7
  ],
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "I trigger the request",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "The client receives response status code of 200",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "The response content type is as expected",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "venues",
      "offset": 22
    }
  ],
  "location": "CollegeFootballStepDefs.apiResourcePathIs(String)"
});
formatter.result({
  "duration": 331400,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "null",
      "offset": 37
    },
    {
      "val": "null",
      "offset": 44
    },
    {
      "val": "null",
      "offset": 51
    },
    {
      "val": "null",
      "offset": 58
    },
    {
      "val": "null",
      "offset": 65
    },
    {
      "val": "null",
      "offset": 72
    },
    {
      "val": "null",
      "offset": 79
    }
  ],
  "location": "CollegeFootballStepDefs.iAddAllRequiredQueryParameters(String,String,String,String,String,String,String)"
});
formatter.result({
  "duration": 129600,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.iTriggerTheRequest()"
});
formatter.result({
  "duration": 676795200,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 44
    }
  ],
  "location": "CollegeFootballStepDefs.theClientReceivesStatusCodeOf(int)"
});
formatter.result({
  "duration": 140381200,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.theResponseContentTypeIsAsExpected()"
});
formatter.result({
  "duration": 629200,
  "status": "passed"
});
formatter.scenario({
  "line": 26,
  "name": "1.1_Verify successful response from all College Football Resources",
  "description": "",
  "id": "college-football-api-acceptance-tests;1.1-verify-successful-response-from-all-college-football-resources;;14",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@TestSet"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "API resource path is \"coaches\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "I add all required query parameters \"null\" \"null\" \"null\" \"null\" \"null\" \"null\" \"meyer\"",
  "matchedColumns": [
    1,
    2,
    3,
    4,
    5,
    6,
    7
  ],
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "I trigger the request",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "The client receives response status code of 200",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "The response content type is as expected",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "coaches",
      "offset": 22
    }
  ],
  "location": "CollegeFootballStepDefs.apiResourcePathIs(String)"
});
formatter.result({
  "duration": 348300,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "null",
      "offset": 37
    },
    {
      "val": "null",
      "offset": 44
    },
    {
      "val": "null",
      "offset": 51
    },
    {
      "val": "null",
      "offset": 58
    },
    {
      "val": "null",
      "offset": 65
    },
    {
      "val": "null",
      "offset": 72
    },
    {
      "val": "meyer",
      "offset": 79
    }
  ],
  "location": "CollegeFootballStepDefs.iAddAllRequiredQueryParameters(String,String,String,String,String,String,String)"
});
formatter.result({
  "duration": 278100,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.iTriggerTheRequest()"
});
formatter.result({
  "duration": 620124100,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 44
    }
  ],
  "location": "CollegeFootballStepDefs.theClientReceivesStatusCodeOf(int)"
});
formatter.result({
  "duration": 953400,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.theResponseContentTypeIsAsExpected()"
});
formatter.result({
  "duration": 558500,
  "status": "passed"
});
formatter.scenario({
  "line": 27,
  "name": "1.1_Verify successful response from all College Football Resources",
  "description": "",
  "id": "college-football-api-acceptance-tests;1.1-verify-successful-response-from-all-college-football-resources;;15",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@TestSet"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "API resource path is \"rankings\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "I add all required query parameters \"regular\" \"2019\" \"null\" \"null\" \"null\" \"null\" \"null\"",
  "matchedColumns": [
    1,
    2,
    3,
    4,
    5,
    6,
    7
  ],
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "I trigger the request",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "The client receives response status code of 200",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "The response content type is as expected",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "rankings",
      "offset": 22
    }
  ],
  "location": "CollegeFootballStepDefs.apiResourcePathIs(String)"
});
formatter.result({
  "duration": 744500,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "regular",
      "offset": 37
    },
    {
      "val": "2019",
      "offset": 47
    },
    {
      "val": "null",
      "offset": 54
    },
    {
      "val": "null",
      "offset": 61
    },
    {
      "val": "null",
      "offset": 68
    },
    {
      "val": "null",
      "offset": 75
    },
    {
      "val": "null",
      "offset": 82
    }
  ],
  "location": "CollegeFootballStepDefs.iAddAllRequiredQueryParameters(String,String,String,String,String,String,String)"
});
formatter.result({
  "duration": 391600,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.iTriggerTheRequest()"
});
formatter.result({
  "duration": 760333600,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 44
    }
  ],
  "location": "CollegeFootballStepDefs.theClientReceivesStatusCodeOf(int)"
});
formatter.result({
  "duration": 256915700,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.theResponseContentTypeIsAsExpected()"
});
formatter.result({
  "duration": 764500,
  "status": "passed"
});
formatter.scenario({
  "line": 28,
  "name": "1.1_Verify successful response from all College Football Resources",
  "description": "",
  "id": "college-football-api-acceptance-tests;1.1-verify-successful-response-from-all-college-football-resources;;16",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@TestSet"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "API resource path is \"venues\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "I add all required query parameters \"null\" \"null\" \"null\" \"null\" \"null\" \"null\" \"null\"",
  "matchedColumns": [
    1,
    2,
    3,
    4,
    5,
    6,
    7
  ],
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "I trigger the request",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "The client receives response status code of 200",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "The response content type is as expected",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "venues",
      "offset": 22
    }
  ],
  "location": "CollegeFootballStepDefs.apiResourcePathIs(String)"
});
formatter.result({
  "duration": 559000,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "null",
      "offset": 37
    },
    {
      "val": "null",
      "offset": 44
    },
    {
      "val": "null",
      "offset": 51
    },
    {
      "val": "null",
      "offset": 58
    },
    {
      "val": "null",
      "offset": 65
    },
    {
      "val": "null",
      "offset": 72
    },
    {
      "val": "null",
      "offset": 79
    }
  ],
  "location": "CollegeFootballStepDefs.iAddAllRequiredQueryParameters(String,String,String,String,String,String,String)"
});
formatter.result({
  "duration": 227900,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.iTriggerTheRequest()"
});
formatter.result({
  "duration": 670913700,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 44
    }
  ],
  "location": "CollegeFootballStepDefs.theClientReceivesStatusCodeOf(int)"
});
formatter.result({
  "duration": 122916800,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.theResponseContentTypeIsAsExpected()"
});
formatter.result({
  "duration": 804100,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 30,
  "name": "1.2_Verify get coaches endpoint returns correct details as per query parameters",
  "description": "",
  "id": "college-football-api-acceptance-tests;1.2-verify-get-coaches-endpoint-returns-correct-details-as-per-query-parameters",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 31,
  "name": "API resource path is \"\u003cEndpoint\u003e\"",
  "keyword": "Given "
});
formatter.step({
  "line": 32,
  "name": "I add all required query parameters to get coaches request \"\u003cFirstName\u003e\" \"\u003cLastName\u003e\" \"\u003cTeam\u003e\" \"\u003cYear\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 33,
  "name": "I trigger the request",
  "keyword": "And "
});
formatter.step({
  "line": 34,
  "name": "The client receives response status code of 200",
  "keyword": "Then "
});
formatter.step({
  "line": 35,
  "name": "The response content type is json",
  "keyword": "And "
});
formatter.step({
  "line": 36,
  "name": "The number of results returned is correct",
  "keyword": "And "
});
formatter.step({
  "line": 37,
  "name": "The response result matches the search criteria",
  "keyword": "And "
});
formatter.examples({
  "line": 38,
  "name": "",
  "description": "",
  "id": "college-football-api-acceptance-tests;1.2-verify-get-coaches-endpoint-returns-correct-details-as-per-query-parameters;",
  "rows": [
    {
      "cells": [
        "Endpoint",
        "FirstName",
        "LastName",
        "Team",
        "Year"
      ],
      "line": 39,
      "id": "college-football-api-acceptance-tests;1.2-verify-get-coaches-endpoint-returns-correct-details-as-per-query-parameters;;1"
    },
    {
      "cells": [
        "coaches",
        "Nick",
        "Saban",
        "Alabama",
        "2019"
      ],
      "line": 40,
      "id": "college-football-api-acceptance-tests;1.2-verify-get-coaches-endpoint-returns-correct-details-as-per-query-parameters;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 40,
  "name": "1.2_Verify get coaches endpoint returns correct details as per query parameters",
  "description": "",
  "id": "college-football-api-acceptance-tests;1.2-verify-get-coaches-endpoint-returns-correct-details-as-per-query-parameters;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@TestSet"
    }
  ]
});
formatter.step({
  "line": 31,
  "name": "API resource path is \"coaches\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 32,
  "name": "I add all required query parameters to get coaches request \"Nick\" \"Saban\" \"Alabama\" \"2019\"",
  "matchedColumns": [
    1,
    2,
    3,
    4
  ],
  "keyword": "When "
});
formatter.step({
  "line": 33,
  "name": "I trigger the request",
  "keyword": "And "
});
formatter.step({
  "line": 34,
  "name": "The client receives response status code of 200",
  "keyword": "Then "
});
formatter.step({
  "line": 35,
  "name": "The response content type is json",
  "keyword": "And "
});
formatter.step({
  "line": 36,
  "name": "The number of results returned is correct",
  "keyword": "And "
});
formatter.step({
  "line": 37,
  "name": "The response result matches the search criteria",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "coaches",
      "offset": 22
    }
  ],
  "location": "CollegeFootballStepDefs.apiResourcePathIs(String)"
});
formatter.result({
  "duration": 7774100,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Nick",
      "offset": 60
    },
    {
      "val": "Saban",
      "offset": 67
    },
    {
      "val": "Alabama",
      "offset": 75
    },
    {
      "val": "2019",
      "offset": 85
    }
  ],
  "location": "CollegeFootballStepDefs.iAddAllRequiredQueryParametersToGetCoachesRequest(String,String,String,String)"
});
formatter.result({
  "duration": 558400,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.iTriggerTheRequest()"
});
formatter.result({
  "duration": 645936600,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 44
    }
  ],
  "location": "CollegeFootballStepDefs.theClientReceivesStatusCodeOf(int)"
});
formatter.result({
  "duration": 1835800,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.theResponseContentTypeIsJson()"
});
formatter.result({
  "duration": 595500,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.theNumberOfResultsReturnedIsCorrect()"
});
formatter.result({
  "duration": 1229098100,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.theResponseResultMatchesTheSearchCriteria()"
});
formatter.result({
  "duration": 242299800,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 42,
  "name": "1.3_Verify 400 error is returned for request missing required parameters",
  "description": "",
  "id": "college-football-api-acceptance-tests;1.3-verify-400-error-is-returned-for-request-missing-required-parameters",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 43,
  "name": "API resource path is \"\u003cEndpoint\u003e\"",
  "keyword": "Given "
});
formatter.step({
  "line": 44,
  "name": "I do not add all required query parameters \"\u003cSeasonType\u003e\" \"\u003cYear\u003e\" \"\u003cWeek\u003e\" \"\u003cTeam\u003e\" \"\u003cConference\u003e\" \"\u003cMatchupTeams\u003e\" \"\u003cCoach\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 45,
  "name": "I trigger the request",
  "keyword": "And "
});
formatter.step({
  "line": 46,
  "name": "The client receives response status code of 400",
  "keyword": "Then "
});
formatter.step({
  "line": 47,
  "name": "The response content type is json",
  "keyword": "And "
});
formatter.step({
  "line": 48,
  "name": "The error message returned is as expected \"\u003cExpectedResult\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "line": 49,
  "name": "",
  "description": "",
  "id": "college-football-api-acceptance-tests;1.3-verify-400-error-is-returned-for-request-missing-required-parameters;",
  "rows": [
    {
      "cells": [
        "Endpoint",
        "SeasonType",
        "Year",
        "Week",
        "Team",
        "Conference",
        "MatchupTeams",
        "Coach",
        "ExpectedResult"
      ],
      "line": 50,
      "id": "college-football-api-acceptance-tests;1.3-verify-400-error-is-returned-for-request-missing-required-parameters;;1"
    },
    {
      "cells": [
        "games",
        "regular",
        "null",
        "null",
        "null",
        "null",
        "null",
        "null",
        "{error:A numeric year parameter must be specified.}"
      ],
      "line": 51,
      "id": "college-football-api-acceptance-tests;1.3-verify-400-error-is-returned-for-request-missing-required-parameters;;2"
    },
    {
      "cells": [
        "drives",
        "regular",
        "null",
        "null",
        "null",
        "null",
        "null",
        "null",
        "{error:A numeric year parameter must be specified.}"
      ],
      "line": 52,
      "id": "college-football-api-acceptance-tests;1.3-verify-400-error-is-returned-for-request-missing-required-parameters;;3"
    },
    {
      "cells": [
        "plays",
        "regular",
        "2019",
        "null",
        "null",
        "null",
        "null",
        "null",
        "{error:A numeric week parameter must be specified.}"
      ],
      "line": 53,
      "id": "college-football-api-acceptance-tests;1.3-verify-400-error-is-returned-for-request-missing-required-parameters;;4"
    },
    {
      "cells": [
        "teams/matchup",
        "null",
        "null",
        "null",
        "null",
        "null",
        "null",
        "null",
        "{error:Two teams must be specified.}"
      ],
      "line": 54,
      "id": "college-football-api-acceptance-tests;1.3-verify-400-error-is-returned-for-request-missing-required-parameters;;5"
    },
    {
      "cells": [
        "rankings",
        "regular",
        "null",
        "null",
        "null",
        "null",
        "null",
        "null",
        "{error:A numeric year parameter must be specified.}"
      ],
      "line": 55,
      "id": "college-football-api-acceptance-tests;1.3-verify-400-error-is-returned-for-request-missing-required-parameters;;6"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 51,
  "name": "1.3_Verify 400 error is returned for request missing required parameters",
  "description": "",
  "id": "college-football-api-acceptance-tests;1.3-verify-400-error-is-returned-for-request-missing-required-parameters;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@TestSet"
    }
  ]
});
formatter.step({
  "line": 43,
  "name": "API resource path is \"games\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 44,
  "name": "I do not add all required query parameters \"regular\" \"null\" \"null\" \"null\" \"null\" \"null\" \"null\"",
  "matchedColumns": [
    1,
    2,
    3,
    4,
    5,
    6,
    7
  ],
  "keyword": "When "
});
formatter.step({
  "line": 45,
  "name": "I trigger the request",
  "keyword": "And "
});
formatter.step({
  "line": 46,
  "name": "The client receives response status code of 400",
  "keyword": "Then "
});
formatter.step({
  "line": 47,
  "name": "The response content type is json",
  "keyword": "And "
});
formatter.step({
  "line": 48,
  "name": "The error message returned is as expected \"{error:A numeric year parameter must be specified.}\"",
  "matchedColumns": [
    8
  ],
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "games",
      "offset": 22
    }
  ],
  "location": "CollegeFootballStepDefs.apiResourcePathIs(String)"
});
formatter.result({
  "duration": 659100,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "regular",
      "offset": 44
    },
    {
      "val": "null",
      "offset": 54
    },
    {
      "val": "null",
      "offset": 61
    },
    {
      "val": "null",
      "offset": 68
    },
    {
      "val": "null",
      "offset": 75
    },
    {
      "val": "null",
      "offset": 82
    },
    {
      "val": "null",
      "offset": 89
    }
  ],
  "location": "CollegeFootballStepDefs.iDoNotAddAllRequiredQueryParameters(String,String,String,String,String,String,String)"
});
formatter.result({
  "duration": 328700,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.iTriggerTheRequest()"
});
formatter.result({
  "duration": 603212700,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "400",
      "offset": 44
    }
  ],
  "location": "CollegeFootballStepDefs.theClientReceivesStatusCodeOf(int)"
});
formatter.result({
  "duration": 1254300,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.theResponseContentTypeIsJson()"
});
formatter.result({
  "duration": 591000,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "{error:A numeric year parameter must be specified.}",
      "offset": 43
    }
  ],
  "location": "CollegeFootballStepDefs.theErrorMessageReturnedIsAsExpected(String)"
});
formatter.result({
  "duration": 3124500,
  "status": "passed"
});
formatter.scenario({
  "line": 52,
  "name": "1.3_Verify 400 error is returned for request missing required parameters",
  "description": "",
  "id": "college-football-api-acceptance-tests;1.3-verify-400-error-is-returned-for-request-missing-required-parameters;;3",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@TestSet"
    }
  ]
});
formatter.step({
  "line": 43,
  "name": "API resource path is \"drives\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 44,
  "name": "I do not add all required query parameters \"regular\" \"null\" \"null\" \"null\" \"null\" \"null\" \"null\"",
  "matchedColumns": [
    1,
    2,
    3,
    4,
    5,
    6,
    7
  ],
  "keyword": "When "
});
formatter.step({
  "line": 45,
  "name": "I trigger the request",
  "keyword": "And "
});
formatter.step({
  "line": 46,
  "name": "The client receives response status code of 400",
  "keyword": "Then "
});
formatter.step({
  "line": 47,
  "name": "The response content type is json",
  "keyword": "And "
});
formatter.step({
  "line": 48,
  "name": "The error message returned is as expected \"{error:A numeric year parameter must be specified.}\"",
  "matchedColumns": [
    8
  ],
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "drives",
      "offset": 22
    }
  ],
  "location": "CollegeFootballStepDefs.apiResourcePathIs(String)"
});
formatter.result({
  "duration": 444500,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "regular",
      "offset": 44
    },
    {
      "val": "null",
      "offset": 54
    },
    {
      "val": "null",
      "offset": 61
    },
    {
      "val": "null",
      "offset": 68
    },
    {
      "val": "null",
      "offset": 75
    },
    {
      "val": "null",
      "offset": 82
    },
    {
      "val": "null",
      "offset": 89
    }
  ],
  "location": "CollegeFootballStepDefs.iDoNotAddAllRequiredQueryParameters(String,String,String,String,String,String,String)"
});
formatter.result({
  "duration": 275400,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.iTriggerTheRequest()"
});
formatter.result({
  "duration": 590834700,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "400",
      "offset": 44
    }
  ],
  "location": "CollegeFootballStepDefs.theClientReceivesStatusCodeOf(int)"
});
formatter.result({
  "duration": 1586200,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.theResponseContentTypeIsJson()"
});
formatter.result({
  "duration": 516700,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "{error:A numeric year parameter must be specified.}",
      "offset": 43
    }
  ],
  "location": "CollegeFootballStepDefs.theErrorMessageReturnedIsAsExpected(String)"
});
formatter.result({
  "duration": 205400,
  "status": "passed"
});
formatter.scenario({
  "line": 53,
  "name": "1.3_Verify 400 error is returned for request missing required parameters",
  "description": "",
  "id": "college-football-api-acceptance-tests;1.3-verify-400-error-is-returned-for-request-missing-required-parameters;;4",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@TestSet"
    }
  ]
});
formatter.step({
  "line": 43,
  "name": "API resource path is \"plays\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 44,
  "name": "I do not add all required query parameters \"regular\" \"2019\" \"null\" \"null\" \"null\" \"null\" \"null\"",
  "matchedColumns": [
    1,
    2,
    3,
    4,
    5,
    6,
    7
  ],
  "keyword": "When "
});
formatter.step({
  "line": 45,
  "name": "I trigger the request",
  "keyword": "And "
});
formatter.step({
  "line": 46,
  "name": "The client receives response status code of 400",
  "keyword": "Then "
});
formatter.step({
  "line": 47,
  "name": "The response content type is json",
  "keyword": "And "
});
formatter.step({
  "line": 48,
  "name": "The error message returned is as expected \"{error:A numeric week parameter must be specified.}\"",
  "matchedColumns": [
    8
  ],
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "plays",
      "offset": 22
    }
  ],
  "location": "CollegeFootballStepDefs.apiResourcePathIs(String)"
});
formatter.result({
  "duration": 354600,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "regular",
      "offset": 44
    },
    {
      "val": "2019",
      "offset": 54
    },
    {
      "val": "null",
      "offset": 61
    },
    {
      "val": "null",
      "offset": 68
    },
    {
      "val": "null",
      "offset": 75
    },
    {
      "val": "null",
      "offset": 82
    },
    {
      "val": "null",
      "offset": 89
    }
  ],
  "location": "CollegeFootballStepDefs.iDoNotAddAllRequiredQueryParameters(String,String,String,String,String,String,String)"
});
formatter.result({
  "duration": 202900,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.iTriggerTheRequest()"
});
formatter.result({
  "duration": 623396300,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "400",
      "offset": 44
    }
  ],
  "location": "CollegeFootballStepDefs.theClientReceivesStatusCodeOf(int)"
});
formatter.result({
  "duration": 1067400,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.theResponseContentTypeIsJson()"
});
formatter.result({
  "duration": 457900,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "{error:A numeric week parameter must be specified.}",
      "offset": 43
    }
  ],
  "location": "CollegeFootballStepDefs.theErrorMessageReturnedIsAsExpected(String)"
});
formatter.result({
  "duration": 143100,
  "status": "passed"
});
formatter.scenario({
  "line": 54,
  "name": "1.3_Verify 400 error is returned for request missing required parameters",
  "description": "",
  "id": "college-football-api-acceptance-tests;1.3-verify-400-error-is-returned-for-request-missing-required-parameters;;5",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@TestSet"
    }
  ]
});
formatter.step({
  "line": 43,
  "name": "API resource path is \"teams/matchup\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 44,
  "name": "I do not add all required query parameters \"null\" \"null\" \"null\" \"null\" \"null\" \"null\" \"null\"",
  "matchedColumns": [
    1,
    2,
    3,
    4,
    5,
    6,
    7
  ],
  "keyword": "When "
});
formatter.step({
  "line": 45,
  "name": "I trigger the request",
  "keyword": "And "
});
formatter.step({
  "line": 46,
  "name": "The client receives response status code of 400",
  "keyword": "Then "
});
formatter.step({
  "line": 47,
  "name": "The response content type is json",
  "keyword": "And "
});
formatter.step({
  "line": 48,
  "name": "The error message returned is as expected \"{error:Two teams must be specified.}\"",
  "matchedColumns": [
    8
  ],
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "teams/matchup",
      "offset": 22
    }
  ],
  "location": "CollegeFootballStepDefs.apiResourcePathIs(String)"
});
formatter.result({
  "duration": 190200,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "null",
      "offset": 44
    },
    {
      "val": "null",
      "offset": 51
    },
    {
      "val": "null",
      "offset": 58
    },
    {
      "val": "null",
      "offset": 65
    },
    {
      "val": "null",
      "offset": 72
    },
    {
      "val": "null",
      "offset": 79
    },
    {
      "val": "null",
      "offset": 86
    }
  ],
  "location": "CollegeFootballStepDefs.iDoNotAddAllRequiredQueryParameters(String,String,String,String,String,String,String)"
});
formatter.result({
  "duration": 109300,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.iTriggerTheRequest()"
});
formatter.result({
  "duration": 575327700,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "400",
      "offset": 44
    }
  ],
  "location": "CollegeFootballStepDefs.theClientReceivesStatusCodeOf(int)"
});
formatter.result({
  "duration": 1110200,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.theResponseContentTypeIsJson()"
});
formatter.result({
  "duration": 623000,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "{error:Two teams must be specified.}",
      "offset": 43
    }
  ],
  "location": "CollegeFootballStepDefs.theErrorMessageReturnedIsAsExpected(String)"
});
formatter.result({
  "duration": 311200,
  "status": "passed"
});
formatter.scenario({
  "line": 55,
  "name": "1.3_Verify 400 error is returned for request missing required parameters",
  "description": "",
  "id": "college-football-api-acceptance-tests;1.3-verify-400-error-is-returned-for-request-missing-required-parameters;;6",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@TestSet"
    }
  ]
});
formatter.step({
  "line": 43,
  "name": "API resource path is \"rankings\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 44,
  "name": "I do not add all required query parameters \"regular\" \"null\" \"null\" \"null\" \"null\" \"null\" \"null\"",
  "matchedColumns": [
    1,
    2,
    3,
    4,
    5,
    6,
    7
  ],
  "keyword": "When "
});
formatter.step({
  "line": 45,
  "name": "I trigger the request",
  "keyword": "And "
});
formatter.step({
  "line": 46,
  "name": "The client receives response status code of 400",
  "keyword": "Then "
});
formatter.step({
  "line": 47,
  "name": "The response content type is json",
  "keyword": "And "
});
formatter.step({
  "line": 48,
  "name": "The error message returned is as expected \"{error:A numeric year parameter must be specified.}\"",
  "matchedColumns": [
    8
  ],
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "rankings",
      "offset": 22
    }
  ],
  "location": "CollegeFootballStepDefs.apiResourcePathIs(String)"
});
formatter.result({
  "duration": 356600,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "regular",
      "offset": 44
    },
    {
      "val": "null",
      "offset": 54
    },
    {
      "val": "null",
      "offset": 61
    },
    {
      "val": "null",
      "offset": 68
    },
    {
      "val": "null",
      "offset": 75
    },
    {
      "val": "null",
      "offset": 82
    },
    {
      "val": "null",
      "offset": 89
    }
  ],
  "location": "CollegeFootballStepDefs.iDoNotAddAllRequiredQueryParameters(String,String,String,String,String,String,String)"
});
formatter.result({
  "duration": 272900,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.iTriggerTheRequest()"
});
formatter.result({
  "duration": 577953200,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "400",
      "offset": 44
    }
  ],
  "location": "CollegeFootballStepDefs.theClientReceivesStatusCodeOf(int)"
});
formatter.result({
  "duration": 3359000,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.theResponseContentTypeIsJson()"
});
formatter.result({
  "duration": 485200,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "{error:A numeric year parameter must be specified.}",
      "offset": 43
    }
  ],
  "location": "CollegeFootballStepDefs.theErrorMessageReturnedIsAsExpected(String)"
});
formatter.result({
  "duration": 199600,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 57,
  "name": "1.4_Verify 400 error is returned for request with parameters formatting errors",
  "description": "",
  "id": "college-football-api-acceptance-tests;1.4-verify-400-error-is-returned-for-request-with-parameters-formatting-errors",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 58,
  "name": "API resource path is \"\u003cEndpoint\u003e\"",
  "keyword": "Given "
});
formatter.step({
  "line": 59,
  "name": "I add all required query parameters \"\u003cSeasonType\u003e\" \"\u003cYear\u003e\" \"\u003cWeek\u003e\" \"\u003cTeam\u003e\" \"\u003cConference\u003e\" \"\u003cMatchupTeams\u003e\" \"\u003cCoach\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 60,
  "name": "I trigger the request",
  "keyword": "And "
});
formatter.step({
  "line": 61,
  "name": "The client receives response status code of 400",
  "keyword": "Then "
});
formatter.step({
  "line": 62,
  "name": "The response content type is json",
  "keyword": "And "
});
formatter.step({
  "line": 63,
  "name": "The error message returned is as expected \"\u003cExpectedResult\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "line": 64,
  "name": "",
  "description": "",
  "id": "college-football-api-acceptance-tests;1.4-verify-400-error-is-returned-for-request-with-parameters-formatting-errors;",
  "rows": [
    {
      "cells": [
        "Endpoint",
        "SeasonType",
        "Year",
        "Week",
        "Team",
        "Conference",
        "MatchupTeams",
        "Coach",
        "ExpectedResult"
      ],
      "line": 65,
      "id": "college-football-api-acceptance-tests;1.4-verify-400-error-is-returned-for-request-with-parameters-formatting-errors;;1"
    },
    {
      "cells": [
        "games",
        "regular",
        "20--19",
        "null",
        "null",
        "null",
        "null",
        "null",
        "{error:A numeric year parameter must be specified.}"
      ],
      "line": 66,
      "id": "college-football-api-acceptance-tests;1.4-verify-400-error-is-returned-for-request-with-parameters-formatting-errors;;2"
    },
    {
      "cells": [
        "drives",
        "reg%%ular",
        "2019",
        "null",
        "null",
        "null",
        "null",
        "null",
        "{error:Invalid season type}"
      ],
      "line": 67,
      "id": "college-football-api-acceptance-tests;1.4-verify-400-error-is-returned-for-request-with-parameters-formatting-errors;;3"
    },
    {
      "cells": [
        "plays",
        "regular",
        "2019",
        "1--2",
        "null",
        "null",
        "null",
        "null",
        "{error:A numeric week parameter must be specified.}"
      ],
      "line": 68,
      "id": "college-football-api-acceptance-tests;1.4-verify-400-error-is-returned-for-request-with-parameters-formatting-errors;;4"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 66,
  "name": "1.4_Verify 400 error is returned for request with parameters formatting errors",
  "description": "",
  "id": "college-football-api-acceptance-tests;1.4-verify-400-error-is-returned-for-request-with-parameters-formatting-errors;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@TestSet"
    }
  ]
});
formatter.step({
  "line": 58,
  "name": "API resource path is \"games\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 59,
  "name": "I add all required query parameters \"regular\" \"20--19\" \"null\" \"null\" \"null\" \"null\" \"null\"",
  "matchedColumns": [
    1,
    2,
    3,
    4,
    5,
    6,
    7
  ],
  "keyword": "When "
});
formatter.step({
  "line": 60,
  "name": "I trigger the request",
  "keyword": "And "
});
formatter.step({
  "line": 61,
  "name": "The client receives response status code of 400",
  "keyword": "Then "
});
formatter.step({
  "line": 62,
  "name": "The response content type is json",
  "keyword": "And "
});
formatter.step({
  "line": 63,
  "name": "The error message returned is as expected \"{error:A numeric year parameter must be specified.}\"",
  "matchedColumns": [
    8
  ],
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "games",
      "offset": 22
    }
  ],
  "location": "CollegeFootballStepDefs.apiResourcePathIs(String)"
});
formatter.result({
  "duration": 853800,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "regular",
      "offset": 37
    },
    {
      "val": "20--19",
      "offset": 47
    },
    {
      "val": "null",
      "offset": 56
    },
    {
      "val": "null",
      "offset": 63
    },
    {
      "val": "null",
      "offset": 70
    },
    {
      "val": "null",
      "offset": 77
    },
    {
      "val": "null",
      "offset": 84
    }
  ],
  "location": "CollegeFootballStepDefs.iAddAllRequiredQueryParameters(String,String,String,String,String,String,String)"
});
formatter.result({
  "duration": 1139700,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.iTriggerTheRequest()"
});
formatter.result({
  "duration": 551989800,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "400",
      "offset": 44
    }
  ],
  "location": "CollegeFootballStepDefs.theClientReceivesStatusCodeOf(int)"
});
formatter.result({
  "duration": 912700,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.theResponseContentTypeIsJson()"
});
formatter.result({
  "duration": 346200,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "{error:A numeric year parameter must be specified.}",
      "offset": 43
    }
  ],
  "location": "CollegeFootballStepDefs.theErrorMessageReturnedIsAsExpected(String)"
});
formatter.result({
  "duration": 175500,
  "status": "passed"
});
formatter.scenario({
  "line": 67,
  "name": "1.4_Verify 400 error is returned for request with parameters formatting errors",
  "description": "",
  "id": "college-football-api-acceptance-tests;1.4-verify-400-error-is-returned-for-request-with-parameters-formatting-errors;;3",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@TestSet"
    }
  ]
});
formatter.step({
  "line": 58,
  "name": "API resource path is \"drives\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 59,
  "name": "I add all required query parameters \"reg%%ular\" \"2019\" \"null\" \"null\" \"null\" \"null\" \"null\"",
  "matchedColumns": [
    1,
    2,
    3,
    4,
    5,
    6,
    7
  ],
  "keyword": "When "
});
formatter.step({
  "line": 60,
  "name": "I trigger the request",
  "keyword": "And "
});
formatter.step({
  "line": 61,
  "name": "The client receives response status code of 400",
  "keyword": "Then "
});
formatter.step({
  "line": 62,
  "name": "The response content type is json",
  "keyword": "And "
});
formatter.step({
  "line": 63,
  "name": "The error message returned is as expected \"{error:Invalid season type}\"",
  "matchedColumns": [
    8
  ],
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "drives",
      "offset": 22
    }
  ],
  "location": "CollegeFootballStepDefs.apiResourcePathIs(String)"
});
formatter.result({
  "duration": 467400,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "reg%%ular",
      "offset": 37
    },
    {
      "val": "2019",
      "offset": 49
    },
    {
      "val": "null",
      "offset": 56
    },
    {
      "val": "null",
      "offset": 63
    },
    {
      "val": "null",
      "offset": 70
    },
    {
      "val": "null",
      "offset": 77
    },
    {
      "val": "null",
      "offset": 84
    }
  ],
  "location": "CollegeFootballStepDefs.iAddAllRequiredQueryParameters(String,String,String,String,String,String,String)"
});
formatter.result({
  "duration": 345500,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.iTriggerTheRequest()"
});
formatter.result({
  "duration": 591463500,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "400",
      "offset": 44
    }
  ],
  "location": "CollegeFootballStepDefs.theClientReceivesStatusCodeOf(int)"
});
formatter.result({
  "duration": 622000,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.theResponseContentTypeIsJson()"
});
formatter.result({
  "duration": 271700,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "{error:Invalid season type}",
      "offset": 43
    }
  ],
  "location": "CollegeFootballStepDefs.theErrorMessageReturnedIsAsExpected(String)"
});
formatter.result({
  "duration": 201700,
  "status": "passed"
});
formatter.scenario({
  "line": 68,
  "name": "1.4_Verify 400 error is returned for request with parameters formatting errors",
  "description": "",
  "id": "college-football-api-acceptance-tests;1.4-verify-400-error-is-returned-for-request-with-parameters-formatting-errors;;4",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@TestSet"
    }
  ]
});
formatter.step({
  "line": 58,
  "name": "API resource path is \"plays\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 59,
  "name": "I add all required query parameters \"regular\" \"2019\" \"1--2\" \"null\" \"null\" \"null\" \"null\"",
  "matchedColumns": [
    1,
    2,
    3,
    4,
    5,
    6,
    7
  ],
  "keyword": "When "
});
formatter.step({
  "line": 60,
  "name": "I trigger the request",
  "keyword": "And "
});
formatter.step({
  "line": 61,
  "name": "The client receives response status code of 400",
  "keyword": "Then "
});
formatter.step({
  "line": 62,
  "name": "The response content type is json",
  "keyword": "And "
});
formatter.step({
  "line": 63,
  "name": "The error message returned is as expected \"{error:A numeric week parameter must be specified.}\"",
  "matchedColumns": [
    8
  ],
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "plays",
      "offset": 22
    }
  ],
  "location": "CollegeFootballStepDefs.apiResourcePathIs(String)"
});
formatter.result({
  "duration": 570300,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "regular",
      "offset": 37
    },
    {
      "val": "2019",
      "offset": 47
    },
    {
      "val": "1--2",
      "offset": 54
    },
    {
      "val": "null",
      "offset": 61
    },
    {
      "val": "null",
      "offset": 68
    },
    {
      "val": "null",
      "offset": 75
    },
    {
      "val": "null",
      "offset": 82
    }
  ],
  "location": "CollegeFootballStepDefs.iAddAllRequiredQueryParameters(String,String,String,String,String,String,String)"
});
formatter.result({
  "duration": 295800,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.iTriggerTheRequest()"
});
formatter.result({
  "duration": 576220900,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "400",
      "offset": 44
    }
  ],
  "location": "CollegeFootballStepDefs.theClientReceivesStatusCodeOf(int)"
});
formatter.result({
  "duration": 1155800,
  "status": "passed"
});
formatter.match({
  "location": "CollegeFootballStepDefs.theResponseContentTypeIsJson()"
});
formatter.result({
  "duration": 476900,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "{error:A numeric week parameter must be specified.}",
      "offset": 43
    }
  ],
  "location": "CollegeFootballStepDefs.theErrorMessageReturnedIsAsExpected(String)"
});
formatter.result({
  "duration": 247600,
  "status": "passed"
});
});