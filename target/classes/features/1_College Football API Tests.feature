@TestSet
Feature: College Football API_Acceptance Tests
  Description: Verify all endpoints work as expected

  Scenario Outline: 1.1_Verify successful response from all College Football Resources
    Given API resource path is "<Endpoint>"
    When I add all required query parameters "<SeasonType>" "<Year>" "<Week>" "<Team>" "<Conference>" "<MatchupTeams>" "<Coach>"
     And I trigger the request
    Then The client receives response status code of 200
    And The response content type is as expected
    Examples:
      | Endpoint      | SeasonType | Year | Week | Team         | Conference | MatchupTeams    | Coach |
      | games         | regular    | 2019 | 1    | Alabama      | SEC        | null            | null  |
      | games/players | regular    | 2019 | 1    | Auburn       | SEC        | null            | null  |
      | games/teams   | regular    | 2019 | 1    | Clemson      | ACC        | null            | null  |
      | drives        | regular    | 2019 | 1    | Alabama      | SEC        | null            | null  |
      | play/types    | null       | null | null | null         | null       | null            | null  |
      | plays         | regular    | 2019 | 1    | Michigan     | null       | null            | null  |
      | roster        | null       | 2019 | null | auburn       | null       | null            | null  |
      | talent        | null       | 2019 | null | null         | null       | null            | null  |
      | teams         | null       | null | null | null         | ACC        | null            | null  |
      | teams/matchup | null       | null | null | null         | null       | auburn,michigan | null  |
      | conferences   | null       | null | null | null         | null       | null            | null  |
      | venues        | null       | null | null | null         | null       | null            | null  |
      | coaches       | null       | 2018 | null | ohio state   | null       | null            | meyer |
      | rankings      | regular    | 2019 | null | null         | null       | null            | null  |
      | venues        | null       | null | null | null         | null       | null            | null  |

  Scenario Outline: 1.2_Verify get coaches endpoint returns correct details as per query parameters
    Given API resource path is "<Endpoint>"
    When I add all required query parameters to get coaches request "<FirstName>" "<LastName>" "<Team>" "<Year>"
    And I trigger the request
    Then The client receives response status code of 200
    And The response content type is json
    And The number of results returned is correct
    And The response result matches the search criteria
    Examples:
      | Endpoint | FirstName | LastName | Team    | Year |
#      | coaches  | Nick      | Saban    | Alabama | 2019 |

  Scenario Outline: 1.3_Verify 400 error is returned for request missing required parameters
    Given API resource path is "<Endpoint>"
    When I do not add all required query parameters "<SeasonType>" "<Year>" "<Week>" "<Team>" "<Conference>" "<MatchupTeams>" "<Coach>"
    And I trigger the request
    Then The client receives response status code of 400
    And The response content type is json
    And The error message returned is as expected "<ExpectedResult>"
    Examples:
      | Endpoint      | SeasonType | Year | Week | Team | Conference | MatchupTeams | Coach | ExpectedResult                                      |
#      | games         | regular    | null | null | null | null       | null         | null  | {error:A numeric year parameter must be specified.} |
#      | drives        | regular    | null | null | null | null       | null         | null  | {error:A numeric year parameter must be specified.} |
#      | plays         | regular    | 2019 | null | null | null       | null         | null  | {error:A numeric week parameter must be specified.} |
#      | teams/matchup | null       | null | null | null | null       | null         | null  | {error:Two teams must be specified.}                |
#      | rankings      | regular    | null | null | null | null       | null         | null  | {error:A numeric year parameter must be specified.} |

  Scenario Outline: 1.4_Verify 400 error is returned for request with parameters formatting errors
    Given API resource path is "<Endpoint>"
    When I add all required query parameters "<SeasonType>" "<Year>" "<Week>" "<Team>" "<Conference>" "<MatchupTeams>" "<Coach>"
    And I trigger the request
    Then The client receives response status code of 400
    And The response content type is json
    And The error message returned is as expected "<ExpectedResult>"
    Examples:
      | Endpoint | SeasonType | Year   | Week | Team | Conference | MatchupTeams | Coach | ExpectedResult                                      |
#      | games    | regular    | 20--19 | null | null | null       | null         | null  | {error:A numeric year parameter must be specified.} |
#      | drives   | reg%%ular  | 2019   | null | null | null       | null         | null  | {error:Invalid season type}                         |
#      | plays    | regular    | 2019   | 1--2 | null | null       | null         | null  | {error:A numeric week parameter must be specified.} |
